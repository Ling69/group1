
import React, { useState } from "react";

const initial = {
        name:'',
        firstname:'',
        age:0
}

export function AddPerson() {

    const [person, setPerson] = useState(initial);

    function handleChange(event:any){
        setPerson({
            ...person,
            [event.target.name]:event.target.value
        });
    }

    const addPerson = () => {
        fetch('http://localhost:4000/person', {
            method:'POST',
            body: JSON.stringify(person)
        }).then(response => response.json())
        .then(data => console.log(data));
    }

    const handlechange = (event:any) => {
        setPerson({
            ...person,
            [event.target.name]: event.targe.value
        });
    }


    return (
        <section>
            <h1>Add Person</h1>

            <form>
                <div className="form-group">
                    <label htmlFor="firstname">Firstname</label>
                    <input type="text" className="form-control" name="firstname" onChange={handleChange} value={person.firstname} />

                </div>
                <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input type="text" className="form-control" name="name" onChange={handleChange} value={person.name} />
                </div>
                <div className="form-group">
                    <label htmlFor="age">Age</label>
                    <input type="number" className="form-control" name="age" onChange={handleChange} value={person.age} />
                </div>
                <button onClick={addPerson} type="submit" className="btn btn-primary" onChange={handlechange}>Submit</button>
            </form>

        </section>
    )
}
